<?php
/**
 * Created by PhpStorm.
 * User: finleysiebert
 * Date: 2019-02-02
 * Time: 21:06
 */

namespace App\Http\Controllers;


use App\Models\Member;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index() {

        $members = Member::orderBy('created_at')->get();

        return view('members.overview', ['members' => $members]);
    }

    public function create(Request $request) {

        if($request->getMethod() == "POST") {

            $request->validate([
                'name' => 'required|present|max:150',
                'email' => 'required|email|present|unique:members',
                'pincode' => 'required'
            ]);

            Member::create([
                'name' => $request->name,
                'email' => $request->email,
                'pin' => $request->pincode
            ]);

            \Session::flash('alert-success', 'De deelnemer is aangemaakt');
            return redirect('members');

        }

        return view('members.manage');
    }
}