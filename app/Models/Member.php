<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: finleysiebert
 * Date: 2019-02-02
 * Time: 21:17
 */

class Member extends Model {

    protected $fillable = [
        'name', 'email', 'pin', 'order_id'
    ];

}