@extends('layouts.login')

@section('content')
    <div class="card card-login mx-auto mt-5">
        <div class="card-body">
            <div class="text-center" style="padding: 20px 0;">
                <img src="{{ asset('images/logo_pvl.png') }}" width="300">
            </div>
            <form method="post" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <div class="form-label-group">
                        <input id="email" type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}" required autofocus>
                        <label for="inputEmail">Email</label>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input id="password" type="password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                               required>
                        <label for="inputPassword">Wachtwoord</label>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"
                                   id="remember" {{ old('remember') ? 'checked' : '' }}>
                            Onthouden
                        </label>
                    </div>
                </div>
                <button class="btn btn-primary btn-block">Login</button>
            </form>
            <br>
            <div class="text-center">
                @if (Route::has('password.request'))
                    <a class="d-block small" href="{{ route('password.request') }}">
                        {{ __('Wachtwoord vergeten?') }}
                    </a>
                @endif
            </div>
        </div>
    </div>
@endsection
