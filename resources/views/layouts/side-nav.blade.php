<ul class="sidebar navbar-nav">
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Deelnemers</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="{{ route('member_overview') }}">Overzicht</a>
            <a class="dropdown-item" href="{{ route('member_create') }}">Nieuwe deelnemer</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-calendar-alt"></i>
            <span>Agenda</span></a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-flask"></i>
            <span>Opdrachten</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Opdrachten</h6>
            <a class="dropdown-item" href="login.html">Overzicht</a>
            <a class="dropdown-item" href="register.html">Nieuwe opdracht</a>
            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header">Kasussen</h6>
            <a class="dropdown-item" href="404.html">Overzicht</a>
            <a class="dropdown-item" href="blank.html">Nieuwe kasus</a>
        </div>
    </li>
</ul>