<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">

</head>
<body>

<body class="bg-dark">

    <div class="container">
        @yield('content')
    </div>

</body>

<!-- Scripts -->
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/sb-admin.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
