@extends('layouts.app')

@section('breadcrumb') {{ Breadcrumbs::render('new-member') }} @endsection

@section('content')
    <h3>Nieuwe deelnemer</h3>
    <div class="row" id="app">
        <div class="col-lg-8 col-md-12 col-xs-12">
            <div class="card">
                <div class="card-header">
                    Voeg een nieuwe deelnemer toe
                </div>
                <div class="card-body">
                    <div class="col">
                        <form autocomplete="off" method="post">
                            @csrf
                        <div class="form-group">
                            <label for="name">Naam</label>
                            <input v-model="name" name="name" autocomplete="false" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" aria-describedby="nameHelp" placeholder="Voer een naam in">
                            <small id="nameHelp" class="form-text text-muted">De naam van de deelnemer</small>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('name') }}</strong></span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="name">E-mail</label>
                            <input v-model="email" autocomplete="false" type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" aria-describedby="emailHelp" placeholder="Voer een e-mail adres in">
                            <small id="emailHelp" class="form-text text-muted">Het e-mail adres van de deelnemer</small>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="name">Pincode</label>
                            <div class="input-group">
                                <input autocomplete="false" name="pincode" v-model="pincode" type="number" class="form-control{{ $errors->has('pincode') ? ' is-invalid' : '' }}" placeholder="Kies een pincode" aria-describedby="pinHelp">
                                <div class="input-group-append">
                                    <button @click="generatePin()" class="btn btn-primary" type="button"><i class="fas fa-code"></i></button>
                                </div>
                                @if ($errors->has('pincode'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('pincode') }}</strong></span>
                                @endif
                            </div>
                            <small id="pinHelp" class="form-text text-muted">
                                Voer een pincode in of laat het systeem een pincode kiezen door op de knop
                                te drukken
                            </small>
                        </div>
                            <button class="btn btn-lg btn-success">Opslaan</button>
                            <span class="form-text text-muted">
                                Deze pincode wordt naar het bovenstaande e-mail verstuurd en kan gebruikt worden om in te loggen op de app.
                            </span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col xs-12">
            <div class="card">
                <div class="card-header">
                    Opdracht toekennen
                </div>
                <div class="card-body">
                    <p>Kies een (of meer) uit onderstaande opdrachten om toe te wijzen aan deze deelnemer</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <!-- TODO: change with local VUEJS file -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                pincode: Math.floor(1000 + Math.random() * 9000),
                name: '{{old('name')}}',
                email: '{{old('email')}}',
                generatePin: function() {
                    this.pincode = Math.floor(1000 + Math.random() * 9000);
                }
            },
        })
    </script>
@endsection
