@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('js/datatables/dataTables.bootstrap4.min.css') }}">
@endsection

@section('breadcrumb') {{ Breadcrumbs::render('members') }} @endsection

@section('content')
    <h3>Deelnemerbeheer</h3>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table datatable" id="datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Naam</th>
                            <th>Aangemaakt</th>
                            <th>Aangepast</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($members as $index => $member)
                            <tr>
                                <td>{{ $index }}</td>
                                <td>{{ $member->name }}</td>
                                <td>{{ Carbon\Carbon::parse($member->created_at)->diffForHumans() }}</td>
                                <td>{{ Carbon\Carbon::parse($member->updated_at)->diffForHumans() }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.js') }}"></script>

    <script>
        $('#datatable').dataTable();
    </script>
@endsection