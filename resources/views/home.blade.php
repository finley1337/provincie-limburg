@extends('layouts.app')

@section('breadcrumb') {{ Breadcrumbs::render('home') }} @endsection

@section('content')

    <div class="row">
        <div class="col-xl-4 col-sm-12 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-flask"></i>
                    </div>
                    <div class="mr-5">23.034 Gebruikers geregistreerd</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-left"></span>
                    <span class="float-right">
                </span>
                </a>
            </div>
        </div>
        <div class="col-xl-4 col-sm-12 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-list"></i>
                    </div>
                    <div class="mr-5">450.456 Huidige opdrachten</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-left"></span>
                    <span class="float-right">
                </span>
                </a>
            </div>
        </div>
        <div class="col-xl-4 col-sm-12 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-shopping-cart"></i>
                    </div>
                    <div class="mr-5">1.346.576 Kasussen</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-left"></span>
                    <span class="float-right">
                </span>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Dashboard notities</div>
                <div class="card-body">
                    <textarea name="" cols="30" rows="7" class="form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Programma activiteiten</div>
                <div class="card-body">
                    nothing yet..
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>

@endsection
