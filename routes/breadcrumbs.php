<?php


Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

Breadcrumbs::for('members', function ($trail) {
    $trail->parent('home', route('home'));
    $trail->push('Deelnemers', route('member_overview'));
});

Breadcrumbs::for('new-member', function ($trail) {
    $trail->parent('members', route('home'));
    $trail->push('Nieuwe deelnemer', route('member_overview'));
});