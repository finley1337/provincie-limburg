<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return ['ok'];
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'members'], function() {

    Route::get('/', 'MemberController@index')->name('member_overview');
    Route::get('/new', 'MemberController@create')->name('member_create');
    Route::post('/new', 'MemberController@create');

});
